﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Hotel
{
    public partial class Form2 : Form
    {
        string Polaczenie_z_baza = "datasource=serwer1505592.home.pl;port=3306;username=18992403_hotel;password=H3Ha0sI9AUp5;charset=utf8";
        string id_pokoju = "";
        string id_goscia = "";
        string _id_rezerwacji = "";
        string _id_uzytkownika = "";
        string dostepnosc = "";
        public Form2(String id) 
        {
            _id_uzytkownika = id;
            InitializeComponent();
            Pokaz_tabele_Pokoje();
            Pokaz_Imie();
        }

        void Pokaz_Imie()
        {
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytaniePracownik = new MySqlCommand("select * from 18992403_hotel.tbUzytkownicy where id_uzytkownika='" + _id_uzytkownika + "';", Polaczenie);
                MySqlDataReader CzytaniePracownik;
                Polaczenie.Open();
                CzytaniePracownik = ZapytaniePracownik.ExecuteReader();

                while (CzytaniePracownik.Read())
                {
                    label19.Text = "Witaj " + CzytaniePracownik["imie"].ToString();                   
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Pokaz_tabele_Pokoje()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            pictureBox8.Enabled = false;
            pictureBox9.Enabled = false;
            listView1.Items.Clear();
            listView1.Columns[0].Width = 30;
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytaniePokoje = new MySqlCommand("select * from 18992403_hotel.tbPokoje where istniejacy_pokoj=1 ORDER BY nr_pietra;", Polaczenie);
                MySqlDataReader CzytaniePokoje;
                Polaczenie.Open();
                CzytaniePokoje = ZapytaniePokoje.ExecuteReader();

                while (CzytaniePokoje.Read())
                {
                    ListViewItem item = new ListViewItem(CzytaniePokoje["id_pokoju"].ToString());
                    item.SubItems.Add(CzytaniePokoje["nr_pietra"].ToString());
                    item.SubItems.Add(CzytaniePokoje["nr_pokoju"].ToString());
                    item.SubItems.Add(CzytaniePokoje["liczba_miejsc"].ToString());

                    listView1.Items.Add(item);
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Pokaz_tabele_Goscie()
        {
            textBox4.Text = ""; textBox7.Text = ""; textBox10.Text = ""; 
            textBox5.Text = ""; textBox8.Text = ""; textBox11.Text = ""; 
            textBox6.Text = ""; textBox9.Text = ""; 

            pictureBox11.Enabled = false;
            pictureBox12.Enabled = false;
            listView2.Items.Clear();
            listView2.Columns[0].Width = 30;
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieGoscie = new MySqlCommand("select * from 18992403_hotel.tbGoscie where aktywny_gosc=1;", Polaczenie);
                MySqlDataReader CzytanieGoscie;
                Polaczenie.Open();
                CzytanieGoscie = ZapytanieGoscie.ExecuteReader();

                while (CzytanieGoscie.Read())
                {
                    ListViewItem item = new ListViewItem(CzytanieGoscie["id_goscia"].ToString());
                    item.SubItems.Add(CzytanieGoscie["imie"].ToString());
                    item.SubItems.Add(CzytanieGoscie["nazwisko"].ToString());
                    item.SubItems.Add(CzytanieGoscie["PESEL"].ToString());
                    item.SubItems.Add(CzytanieGoscie["ulica"].ToString());
                    item.SubItems.Add(CzytanieGoscie["nr_domu"].ToString());
                    item.SubItems.Add(CzytanieGoscie["miasto"].ToString());
                    item.SubItems.Add(CzytanieGoscie["kod_pocztowy"].ToString());
                    item.SubItems.Add(CzytanieGoscie["telefon"].ToString());

                    listView2.Items.Add(item);
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Pokaz_tabele_Rezerwacje()
        {            
            listView3.Items.Clear();
            listView3.Columns[0].Width = 30;
            pictureBox13.Enabled = false;
            pictureBox14.Enabled = false;
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieRezerwacje = new MySqlCommand("select * from 18992403_hotel.tbRezerwacje INNER JOIN 18992403_hotel.tbPokoje ON tbRezerwacje.Id_pokoju=tbPokoje.id_pokoju INNER JOIN 18992403_hotel.tbGoscie ON tbRezerwacje.Id_goscia=tbGoscie.id_goscia where Istniejaca_rezerwacja=1 ORDER BY Id_rezerwacji;", Polaczenie);
                MySqlDataReader CzytanieRezerwacje;
                Polaczenie.Open();
                CzytanieRezerwacje = ZapytanieRezerwacje.ExecuteReader();

                while (CzytanieRezerwacje.Read())
                {
                    ListViewItem item = new ListViewItem(CzytanieRezerwacje["Id_rezerwacji"].ToString());
                    item.SubItems.Add(CzytanieRezerwacje["nr_pokoju"].ToString());
                    item.SubItems.Add(CzytanieRezerwacje["Liczba_osob"].ToString());
                    item.SubItems.Add(CzytanieRezerwacje["Data_rozpoczecia"].ToString());
                    item.SubItems.Add(CzytanieRezerwacje["Data_zakonczenia"].ToString());
                    item.SubItems.Add(CzytanieRezerwacje["imie"].ToString() + " " + CzytanieRezerwacje["nazwisko"].ToString());
                    listView3.Items.Add(item);
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            id_pokoju = listView1.SelectedItems[0].SubItems[0].Text;
            textBox1.Text = listView1.SelectedItems[0].SubItems[1].Text;
            textBox2.Text = listView1.SelectedItems[0].SubItems[2].Text;
            textBox3.Text = listView1.SelectedItems[0].SubItems[3].Text;
            pictureBox8.Enabled = true;
            pictureBox9.Enabled = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.BackColor = Color.White;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.BackColor = Color.White;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            textBox3.BackColor = Color.White;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 FormaLogowanie = new Form1();
            FormaLogowanie.Show();
            MessageBox.Show("Pomyślnie wylogowano");
        }

        private void listView2_MouseClick(object sender, MouseEventArgs e)
        {
            id_goscia = listView2.SelectedItems[0].SubItems[0].Text;
            textBox4.Text = listView2.SelectedItems[0].SubItems[1].Text;
            textBox5.Text = listView2.SelectedItems[0].SubItems[2].Text;
            textBox6.Text = listView2.SelectedItems[0].SubItems[3].Text;
            textBox7.Text = listView2.SelectedItems[0].SubItems[4].Text;
            textBox8.Text = listView2.SelectedItems[0].SubItems[5].Text;
            textBox9.Text = listView2.SelectedItems[0].SubItems[6].Text;
            textBox10.Text = listView2.SelectedItems[0].SubItems[7].Text;
            textBox11.Text = listView2.SelectedItems[0].SubItems[8].Text;
            pictureBox11.Enabled = true;
            pictureBox12.Enabled = true;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if ((textBox4.Text != "") && (textBox5.Text != "") && (textBox6.Text != "") && (textBox7.Text != "") && (textBox8.Text != "") && (textBox9.Text != "") && (textBox10.Text != "") && (textBox11.Text != ""))
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieGoscie = new MySqlCommand("insert into 18992403_hotel.tbGoscie (imie,nazwisko,PESEL,ulica,nr_domu,miasto,kod_pocztowy,telefon) values('" + this.textBox4.Text + "','" + this.textBox5.Text + "','" + this.textBox6.Text + "','" + this.textBox7.Text + "','" + this.textBox8.Text + "','" + this.textBox9.Text + "','" + this.textBox10.Text + "','" + this.textBox11.Text + "') ;", Polaczenie);
                MySqlDataReader CzytanieGoscie;

                try
                {
                    Polaczenie.Open();
                    CzytanieGoscie = ZapytanieGoscie.ExecuteReader();
                    MessageBox.Show("Zapisano");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Goscie();
            }
            else
            {
                if (textBox4.Text == "") textBox4.BackColor = Color.Salmon;
                if (textBox5.Text == "") textBox5.BackColor = Color.Salmon;
                if (textBox6.Text == "") textBox6.BackColor = Color.Salmon;
                if (textBox7.Text == "") textBox7.BackColor = Color.Salmon;
                if (textBox8.Text == "") textBox8.BackColor = Color.Salmon;
                if (textBox9.Text == "") textBox9.BackColor = Color.Salmon;
                if (textBox10.Text == "") textBox10.BackColor = Color.Salmon;
                if (textBox11.Text == "") textBox11.BackColor = Color.Salmon;
                MessageBox.Show("Wprowadź dane");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if ((textBox4.Text != "") && (textBox5.Text != "") && (textBox6.Text != "") && (textBox7.Text != "") && (textBox8.Text != "") && (textBox9.Text != "") && (textBox10.Text != "") && (textBox11.Text != ""))
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieGoscie = new MySqlCommand("update 18992403_hotel.tbGoscie set id_goscia = '" + id_goscia + "',imie = '" + textBox4.Text + "',nazwisko = '" + textBox5.Text + "',PESEL = '" + textBox6.Text + "',ulica = '" + textBox7.Text + "',nr_domu = '" + textBox8.Text + "',miasto = '" + textBox9.Text + "',kod_pocztowy = '" + textBox10.Text + "',telefon = '" + textBox11.Text + "' where id_goscia = '" + id_goscia + "'; ", Polaczenie);
                MySqlDataReader CzytanieGoscie;

                try
                {
                    Polaczenie.Open();
                    CzytanieGoscie = ZapytanieGoscie.ExecuteReader();
                    MessageBox.Show("Zaktualizowano");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Goscie();
            }
            else
            {
                if (textBox4.Text == "") textBox4.BackColor = Color.Salmon;
                if (textBox5.Text == "") textBox5.BackColor = Color.Salmon;
                if (textBox6.Text == "") textBox6.BackColor = Color.Salmon;
                if (textBox7.Text == "") textBox7.BackColor = Color.Salmon;
                if (textBox8.Text == "") textBox8.BackColor = Color.Salmon;
                if (textBox9.Text == "") textBox9.BackColor = Color.Salmon;
                if (textBox10.Text == "") textBox10.BackColor = Color.Salmon;
                if (textBox11.Text == "") textBox11.BackColor = Color.Salmon;
                MessageBox.Show("Wprowadź dane");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if ((textBox4.Text != "") && (textBox5.Text != "") && (textBox6.Text != "") && (textBox7.Text != "") && (textBox8.Text != "") && (textBox9.Text != "") && (textBox10.Text != "") && (textBox11.Text != ""))
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieGoscie = new MySqlCommand("update 18992403_hotel.tbGoscie set id_goscia = '" + id_goscia + "',imie = '" + textBox4.Text + "',nazwisko = '" + textBox5.Text + "',PESEL = '" + textBox6.Text + "',ulica = '" + textBox7.Text + "',nr_domu = '" + textBox8.Text + "',miasto = '" + textBox9.Text + "',kod_pocztowy = '" + textBox10.Text + "',telefon = '" + textBox11.Text + "',aktywny_gosc=0 where id_goscia = '" + id_goscia + "'; ", Polaczenie);
                MySqlDataReader CzytanieGoscie;

                try
                {
                    Polaczenie.Open();
                    CzytanieGoscie = ZapytanieGoscie.ExecuteReader();
                    MessageBox.Show("Usunięto");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Goscie();
            }
            else
            {
                if (textBox4.Text == "") textBox4.BackColor = Color.Salmon;
                if (textBox5.Text == "") textBox5.BackColor = Color.Salmon;
                if (textBox6.Text == "") textBox6.BackColor = Color.Salmon;
                if (textBox7.Text == "") textBox7.BackColor = Color.Salmon;
                if (textBox8.Text == "") textBox8.BackColor = Color.Salmon;
                if (textBox9.Text == "") textBox9.BackColor = Color.Salmon;
                if (textBox10.Text == "") textBox10.BackColor = Color.Salmon;
                if (textBox11.Text == "") textBox11.BackColor = Color.Salmon;
                MessageBox.Show("Wprowadź dane");
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            monthCalendar1.SelectionRange = new SelectionRange(dateTimePicker1.Value.Date, dateTimePicker2.Value.Date);
        }
        
        private void listView3_MouseClick(object sender, MouseEventArgs e)
        {
            _id_rezerwacji = listView3.SelectedItems[0].SubItems[0].Text;                   
            pictureBox13.Enabled = true;
            pictureBox14.Enabled = true;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
            Pokaz_tabele_Pokoje();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
            Pokaz_tabele_Goscie();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
            Pokaz_tabele_Rezerwacje();
            pictureBox19.Enabled = false;
        }
        
        private void pictureBox7_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text != "") && (textBox2.Text != "") && (textBox3.Text != ""))
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytaniePokoje = new MySqlCommand("insert into 18992403_hotel.tbPokoje (nr_pokoju,nr_pietra,liczba_miejsc) values('" + this.textBox2.Text + "','" + this.textBox1.Text + "','" + this.textBox3.Text + "') ;", Polaczenie);
                MySqlDataReader CzytaniePokoje;

                try
                {
                    Polaczenie.Open();
                    CzytaniePokoje = ZapytaniePokoje.ExecuteReader();
                    MessageBox.Show("Zapisano");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Pokoje();
            }
            else
            {
                if (textBox1.Text == "") textBox1.BackColor = Color.Salmon;
                if (textBox2.Text == "") textBox2.BackColor = Color.Salmon;
                if (textBox3.Text == "") textBox3.BackColor = Color.Salmon;
                MessageBox.Show("Wprowadź dane");
            }
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text != "") && (textBox2.Text != "") && (textBox3.Text != ""))
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytaniePokoje = new MySqlCommand("update 18992403_hotel.tbPokoje set id_pokoju = '" + id_pokoju + "',nr_pokoju = '" + textBox2.Text + "',nr_pietra = '" + textBox1.Text + "',liczba_miejsc = '" + textBox3.Text + "' where id_pokoju = '" + id_pokoju + "'; ", Polaczenie);
                MySqlDataReader CzytaniePokoje;

                try
                {
                    Polaczenie.Open();
                    CzytaniePokoje = ZapytaniePokoje.ExecuteReader();
                    MessageBox.Show("Zaktualizowano");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Pokoje();
            }
            else
            {
                if (textBox1.Text == "") textBox1.BackColor = Color.Salmon;
                if (textBox2.Text == "") textBox2.BackColor = Color.Salmon;
                if (textBox3.Text == "") textBox3.BackColor = Color.Salmon;
                MessageBox.Show("Wprowadź dane");
            }
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text != "") && (textBox2.Text != "") && (textBox3.Text != ""))
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytaniePokoje = new MySqlCommand("update 18992403_hotel.tbPokoje set istniejacy_pokoj=0 where id_pokoju = '" + id_pokoju + "'; ", Polaczenie);
                MySqlDataReader CzytaniePokoje;

                try
                {
                    Polaczenie.Open();
                    CzytaniePokoje = ZapytaniePokoje.ExecuteReader();
                    MessageBox.Show("Usunięto");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Pokoje();
            }
            else
            {
                if (textBox1.Text == "") textBox1.BackColor = Color.Salmon;
                if (textBox2.Text == "") textBox2.BackColor = Color.Salmon;
                if (textBox3.Text == "") textBox3.BackColor = Color.Salmon;
                MessageBox.Show("Wprowadź dane");
            }
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            if ((textBox4.Text != "") && (textBox5.Text != "") && (textBox6.Text != "") && (textBox7.Text != "") && (textBox8.Text != "") && (textBox9.Text != "") && (textBox10.Text != "") && (textBox11.Text != ""))
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieGoscie = new MySqlCommand("insert into 18992403_hotel.tbGoscie (imie,nazwisko,PESEL,ulica,nr_domu,miasto,kod_pocztowy,telefon) values('" + this.textBox4.Text + "','" + this.textBox5.Text + "','" + this.textBox6.Text + "','" + this.textBox7.Text + "','" + this.textBox8.Text + "','" + this.textBox9.Text + "','" + this.textBox10.Text + "','" + this.textBox11.Text + "') ;", Polaczenie);
                MySqlDataReader CzytanieGoscie;

                try
                {
                    Polaczenie.Open();
                    CzytanieGoscie = ZapytanieGoscie.ExecuteReader();
                    MessageBox.Show("Zapisano");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Goscie();
            }
            else
            {
                if (textBox4.Text == "") textBox4.BackColor = Color.Salmon;
                if (textBox5.Text == "") textBox5.BackColor = Color.Salmon;
                if (textBox6.Text == "") textBox6.BackColor = Color.Salmon;
                if (textBox7.Text == "") textBox7.BackColor = Color.Salmon;
                if (textBox8.Text == "") textBox8.BackColor = Color.Salmon;
                if (textBox9.Text == "") textBox9.BackColor = Color.Salmon;
                if (textBox10.Text == "") textBox10.BackColor = Color.Salmon;
                if (textBox11.Text == "") textBox11.BackColor = Color.Salmon;
                MessageBox.Show("Wprowadź dane");
            }
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            if ((textBox4.Text != "") && (textBox5.Text != "") && (textBox6.Text != "") && (textBox7.Text != "") && (textBox8.Text != "") && (textBox9.Text != "") && (textBox10.Text != "") && (textBox11.Text != ""))
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieGoscie = new MySqlCommand("update 18992403_hotel.tbGoscie set id_goscia = '" + id_goscia + "',imie = '" + textBox4.Text + "',nazwisko = '" + textBox5.Text + "',PESEL = '" + textBox6.Text + "',ulica = '" + textBox7.Text + "',nr_domu = '" + textBox8.Text + "',miasto = '" + textBox9.Text + "',kod_pocztowy = '" + textBox10.Text + "',telefon = '" + textBox11.Text + "' where id_goscia = '" + id_goscia + "'; ", Polaczenie);
                MySqlDataReader CzytanieGoscie;

                try
                {
                    Polaczenie.Open();
                    CzytanieGoscie = ZapytanieGoscie.ExecuteReader();
                    MessageBox.Show("Zaktualizowano");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Goscie();
            }
            else
            {
                if (textBox4.Text == "") textBox4.BackColor = Color.Salmon;
                if (textBox5.Text == "") textBox5.BackColor = Color.Salmon;
                if (textBox6.Text == "") textBox6.BackColor = Color.Salmon;
                if (textBox7.Text == "") textBox7.BackColor = Color.Salmon;
                if (textBox8.Text == "") textBox8.BackColor = Color.Salmon;
                if (textBox9.Text == "") textBox9.BackColor = Color.Salmon;
                if (textBox10.Text == "") textBox10.BackColor = Color.Salmon;
                if (textBox11.Text == "") textBox11.BackColor = Color.Salmon;
                MessageBox.Show("Wprowadź dane");
            }
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {           
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieGoscie = new MySqlCommand("update 18992403_hotel.tbGoscie set aktywny_gosc=0 where id_goscia = '" + id_goscia + "'; ", Polaczenie);
                MySqlDataReader CzytanieGoscie;

                try
                {
                    Polaczenie.Open();
                    CzytanieGoscie = ZapytanieGoscie.ExecuteReader();
                    MessageBox.Show("Usunięto");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Goscie();                     
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            Form3 FormaTrzecia = new Form3(_id_rezerwacji);
            FormaTrzecia.ShowDialog();
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand ZapytanieRezerwacja = new MySqlCommand("update 18992403_hotel.tbRezerwacje set Istniejaca_rezerwacja=0 where Id_rezerwacji='" + _id_rezerwacji + "'; ", Polaczenie);
            MySqlDataReader CzytanieRezerwacja;

            try
            {
                Polaczenie.Open();
                CzytanieRezerwacja = ZapytanieRezerwacja.ExecuteReader();
                MessageBox.Show("Usunięto");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Polaczenie.Close();
        }

        private void pictureBox15_Click(object sender, EventArgs e)
        {
            label17.Text = "Aktualne rezerwacje";
            int year;
            int month;
            int day;

            listView3.Items.Clear();
            listView3.Columns[0].Width = 30;
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieRezerwacje = new MySqlCommand("select * from 18992403_hotel.tbRezerwacje INNER JOIN 18992403_hotel.tbPokoje ON tbRezerwacje.Id_pokoju=tbPokoje.id_pokoju INNER JOIN 18992403_hotel.tbGoscie ON tbRezerwacje.Id_goscia=tbGoscie.id_goscia where Istniejaca_rezerwacja=1 ORDER BY Id_rezerwacji;", Polaczenie);
                MySqlDataReader CzytanieRezerwacje;
                Polaczenie.Open();
                CzytanieRezerwacje = ZapytanieRezerwacje.ExecuteReader();
                while (CzytanieRezerwacje.Read())
                {
                    year = Convert.ToInt32(CzytanieRezerwacje["Data_rozpoczecia"].ToString().Remove(4));
                    month = Convert.ToInt32(CzytanieRezerwacje["Data_rozpoczecia"].ToString().Remove(0, 5).Remove(2));
                    day = Convert.ToInt32(CzytanieRezerwacje["Data_rozpoczecia"].ToString().Remove(0, 8));
                    DateTime poczatek = new DateTime(year, month, day);

                    year = Convert.ToInt32(CzytanieRezerwacje["Data_zakonczenia"].ToString().Remove(4));
                    month = Convert.ToInt32(CzytanieRezerwacje["Data_zakonczenia"].ToString().Remove(0, 5).Remove(2));
                    day = Convert.ToInt32(CzytanieRezerwacje["Data_zakonczenia"].ToString().Remove(0, 8));
                    DateTime zakonczenie = new DateTime(year, month, day);

                    if ((poczatek <= DateTime.Today.Date) && (zakonczenie > DateTime.Today.Date))
                    {
                        ListViewItem item = new ListViewItem(CzytanieRezerwacje["Id_rezerwacji"].ToString());
                        item.SubItems.Add(CzytanieRezerwacje["nr_pokoju"].ToString());
                        item.SubItems.Add(CzytanieRezerwacje["Liczba_osob"].ToString());
                        item.SubItems.Add(CzytanieRezerwacje["Data_rozpoczecia"].ToString());
                        item.SubItems.Add(CzytanieRezerwacje["Data_zakonczenia"].ToString());
                        item.SubItems.Add(CzytanieRezerwacje["imie"].ToString() + " " + CzytanieRezerwacje["nazwisko"].ToString());
                        listView3.Items.Add(item);
                    }
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            Pokaz_tabele_Rezerwacje();
            label17.Text = "Wszystkie rezerwacje";
        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            int year;
            int month;
            int day;
            listView4.Items.Clear();
            listView4.Columns[0].Width = 30;
            label18.Text = "Aktualnie zajęte i wolne pokoje";

            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytaniePokoje = new MySqlCommand("select * from 18992403_hotel.tbPokoje where istniejacy_pokoj=1 ORDER BY nr_pietra;", Polaczenie);
                MySqlDataReader CzytaniePokoje;

                MySqlConnection Polaczenie2 = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieRezerwacje = new MySqlCommand("select * from 18992403_hotel.tbRezerwacje where Istniejaca_rezerwacja=1;", Polaczenie2);
                MySqlDataReader CzytanieRezerwacje;

                Polaczenie.Open();
                CzytaniePokoje = ZapytaniePokoje.ExecuteReader();

                Polaczenie2.Open();
                CzytanieRezerwacje = ZapytanieRezerwacje.ExecuteReader();

                while (CzytaniePokoje.Read())
                {

                    ListViewItem item = new ListViewItem(CzytaniePokoje["id_pokoju"].ToString());
                    item.UseItemStyleForSubItems = false;
                    item.SubItems.Add(CzytaniePokoje["nr_pietra"].ToString());
                    item.SubItems.Add(CzytaniePokoje["nr_pokoju"].ToString());
                    item.SubItems.Add(CzytaniePokoje["liczba_miejsc"].ToString());

                    while (CzytanieRezerwacje.Read())
                    {
                        if (CzytanieRezerwacje["Id_pokoju"].ToString() == CzytaniePokoje["id_pokoju"].ToString())
                        {
                            year = Convert.ToInt32(CzytanieRezerwacje["Data_rozpoczecia"].ToString().Remove(4));
                            month = Convert.ToInt32(CzytanieRezerwacje["Data_rozpoczecia"].ToString().Remove(0, 5).Remove(2));
                            day = Convert.ToInt32(CzytanieRezerwacje["Data_rozpoczecia"].ToString().Remove(0, 8));
                            DateTime poczatek = new DateTime(year, month, day);

                            year = Convert.ToInt32(CzytanieRezerwacje["Data_zakonczenia"].ToString().Remove(4));
                            month = Convert.ToInt32(CzytanieRezerwacje["Data_zakonczenia"].ToString().Remove(0, 5).Remove(2));
                            day = Convert.ToInt32(CzytanieRezerwacje["Data_zakonczenia"].ToString().Remove(0, 8));
                            DateTime zakonczenie = new DateTime(year, month, day);

                            if ((poczatek <= DateTime.Today.Date) && (zakonczenie > DateTime.Today.Date))
                            {
                                item.SubItems.Add("Zajęty").BackColor = Color.Tomato;
                            }
                        }
                    }
                    item.SubItems.Add("Wolny").BackColor = Color.Lime;
                    Polaczenie2.Close();
                    Polaczenie2.Open();
                    CzytanieRezerwacje = ZapytanieRezerwacje.ExecuteReader();
                    listView4.Items.Add(item);

                }
                Polaczenie.Close();
                Polaczenie2.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox18_Click(object sender, EventArgs e)
        {
            label18.Text = "Wolne pokoje w zadanym terminie";
            int year;
            int month;
            int day;
            string dostepnosc = "";
            listView4.Items.Clear();
            listView4.Columns[0].Width = 30;

            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytaniePokoje = new MySqlCommand("select * from 18992403_hotel.tbPokoje where istniejacy_pokoj=1 ORDER BY nr_pietra;", Polaczenie);
                MySqlDataReader CzytaniePokoje;

                MySqlConnection Polaczenie2 = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieRezerwacje = new MySqlCommand("select * from 18992403_hotel.tbRezerwacje where Istniejaca_rezerwacja=1;", Polaczenie2);
                MySqlDataReader CzytanieRezerwacje;

                Polaczenie.Open();
                CzytaniePokoje = ZapytaniePokoje.ExecuteReader();

                Polaczenie2.Open();
                CzytanieRezerwacje = ZapytanieRezerwacje.ExecuteReader();

                while (CzytaniePokoje.Read())
                {
                    ListViewItem item = new ListViewItem(CzytaniePokoje["id_pokoju"].ToString());
                    item.UseItemStyleForSubItems = false;
                    item.SubItems.Add(CzytaniePokoje["nr_pietra"].ToString());
                    item.SubItems.Add(CzytaniePokoje["nr_pokoju"].ToString());
                    item.SubItems.Add(CzytaniePokoje["liczba_miejsc"].ToString());

                    while (CzytanieRezerwacje.Read())
                    {
                        if (CzytanieRezerwacje["Id_pokoju"].ToString() == CzytaniePokoje["id_pokoju"].ToString())
                        {
                            year = Convert.ToInt32(CzytanieRezerwacje["Data_rozpoczecia"].ToString().Remove(4));
                            month = Convert.ToInt32(CzytanieRezerwacje["Data_rozpoczecia"].ToString().Remove(0, 5).Remove(2));
                            day = Convert.ToInt32(CzytanieRezerwacje["Data_rozpoczecia"].ToString().Remove(0, 8));
                            DateTime poczatek = new DateTime(year, month, day);

                            year = Convert.ToInt32(CzytanieRezerwacje["Data_zakonczenia"].ToString().Remove(4));
                            month = Convert.ToInt32(CzytanieRezerwacje["Data_zakonczenia"].ToString().Remove(0, 5).Remove(2));
                            day = Convert.ToInt32(CzytanieRezerwacje["Data_zakonczenia"].ToString().Remove(0, 8));
                            DateTime zakonczenie = new DateTime(year, month, day);

                            if ((poczatek > dateTimePicker1.Value.Date))
                            {
                                if ((poczatek < dateTimePicker2.Value.Date))
                                {
                                    dostepnosc = "Zajęty";
                                }
                                else
                                {
                                    dostepnosc = "Wolny";
                                }
                            }
                            else
                            {
                                if ((zakonczenie > dateTimePicker1.Value.Date))
                                {
                                    dostepnosc = "Zajęty";
                                }
                                else
                                {
                                    dostepnosc = "Wolny";
                                }
                            }
                            if (dostepnosc == "Zajęty")
                            {
                                break;
                            }
                        }
                    }
                    if ((dostepnosc != "Zajęty") && (CzytaniePokoje["liczba_miejsc"].ToString() == numericUpDown1.Value.ToString()))
                    {
                        item.SubItems.Add("Wolny").BackColor = Color.Lime;
                        listView4.Items.Add(item);
                    }
                    dostepnosc = "";
                    Polaczenie2.Close();
                    Polaczenie2.Open();
                    CzytanieRezerwacje = ZapytanieRezerwacje.ExecuteReader();
                }
                Polaczenie.Close();
                Polaczenie2.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox19_Click(object sender, EventArgs e)
        {
            if (dostepnosc == "Wolny")
            {
                string nr_pokoju = listView4.SelectedItems[0].SubItems[2].Text;
                string liczba_miejsc = listView4.SelectedItems[0].SubItems[3].Text;
                Form4 FormaCzwarta = new Form4(id_pokoju, nr_pokoju, liczba_miejsc, dateTimePicker1.Text, dateTimePicker2.Text);
                FormaCzwarta.ShowDialog();
            }
            else
            {
                MessageBox.Show("Wybrany pokój zajęty");
            }
        }

        private void listView4_MouseClick(object sender, MouseEventArgs e)
        {
            id_pokoju = listView4.SelectedItems[0].SubItems[0].Text;
            dostepnosc = listView4.SelectedItems[0].SubItems[4].Text;
            pictureBox19.Enabled = true;            
        }

        private void pictureBox20_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 FormaLogowanie = new Form1();
            FormaLogowanie.Show();
            MessageBox.Show("Pomyślnie wylogowano");
        }

        private void pictureBox21_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox22_Click(object sender, EventArgs e)
        {
            Form5 FormaPiata = new Form5();
            FormaPiata.Show();
        }
    }
}
