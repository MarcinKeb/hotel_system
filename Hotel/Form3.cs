﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Hotel
{
    public partial class Form3 : Form
    {
        string Polaczenie_z_baza = "datasource=serwer1505592.home.pl;port=3306;username=18992403_hotel;password=H3Ha0sI9AUp5;charset=utf8";
        string _id_rezerwacji = "";
        string id_pokoju = "";
        string id_goscia = "";
        public Form3(string id_rezerwacji)
        {
            _id_rezerwacji = id_rezerwacji;
            InitializeComponent();
            Pokaz_rezerwacje();
            Pokaz_tabele_Pokoje();
            Pokaz_tabele_Goscie();
        }

        void Pokaz_tabele_Pokoje()
        {
            listView1.Items.Clear();
            listView1.Columns[0].Width = 30;
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytaniePokoje = new MySqlCommand("select * from 18992403_hotel.tbPokoje where istniejacy_pokoj=1 ORDER BY nr_pietra;", Polaczenie);
                MySqlDataReader CzytaniePokoje;
                Polaczenie.Open();
                CzytaniePokoje = ZapytaniePokoje.ExecuteReader();

                while (CzytaniePokoje.Read())
                {
                    ListViewItem item = new ListViewItem(CzytaniePokoje["id_pokoju"].ToString());
                    item.SubItems.Add(CzytaniePokoje["nr_pietra"].ToString());
                    item.SubItems.Add(CzytaniePokoje["nr_pokoju"].ToString());
                    item.SubItems.Add(CzytaniePokoje["liczba_miejsc"].ToString());

                    listView1.Items.Add(item);
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Pokaz_tabele_Goscie()
        {
            listView2.Items.Clear();
            listView2.Columns[0].Width = 30;
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieGoscie = new MySqlCommand("select * from 18992403_hotel.tbGoscie where aktywny_gosc=1;", Polaczenie);
                MySqlDataReader CzytanieGoscie;
                Polaczenie.Open();
                CzytanieGoscie = ZapytanieGoscie.ExecuteReader();

                while (CzytanieGoscie.Read())
                {
                    ListViewItem item = new ListViewItem(CzytanieGoscie["id_goscia"].ToString());
                    item.SubItems.Add(CzytanieGoscie["imie"].ToString());
                    item.SubItems.Add(CzytanieGoscie["nazwisko"].ToString());
                    item.SubItems.Add(CzytanieGoscie["PESEL"].ToString());
                    item.SubItems.Add(CzytanieGoscie["ulica"].ToString());
                    item.SubItems.Add(CzytanieGoscie["nr_domu"].ToString());
                    item.SubItems.Add(CzytanieGoscie["miasto"].ToString());
                    item.SubItems.Add(CzytanieGoscie["kod_pocztowy"].ToString());
                    item.SubItems.Add(CzytanieGoscie["telefon"].ToString());

                    listView2.Items.Add(item);
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Pokaz_rezerwacje()
        {
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieRezerwacja = new MySqlCommand("select * from 18992403_hotel.tbRezerwacje INNER JOIN 18992403_hotel.tbPokoje ON tbRezerwacje.Id_pokoju=tbPokoje.id_pokoju INNER JOIN 18992403_hotel.tbGoscie ON tbRezerwacje.Id_goscia=tbGoscie.id_goscia where Id_rezerwacji='" + _id_rezerwacji + "';", Polaczenie);
                MySqlDataReader CzytanieRezerwacja;
                Polaczenie.Open();
                CzytanieRezerwacja = ZapytanieRezerwacja.ExecuteReader();

                while (CzytanieRezerwacja.Read())
                {
                   textBox1.Text = CzytanieRezerwacja["nr_pokoju"].ToString();
                   numericUpDown1.Value = Convert.ToInt32(CzytanieRezerwacja["Liczba_osob"].ToString());
                   dateTimePicker1.Text = CzytanieRezerwacja["Data_rozpoczecia"].ToString();
                   dateTimePicker2.Text = CzytanieRezerwacja["Data_zakonczenia"].ToString();
                   textBox5.Text = CzytanieRezerwacja["imie"].ToString() + " " + CzytanieRezerwacja["Nazwisko"].ToString();
                   id_goscia = CzytanieRezerwacja["id_goscia"].ToString();
                   id_pokoju = CzytanieRezerwacja["id_pokoju"].ToString();
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            id_pokoju = listView1.SelectedItems[0].SubItems[0].Text;
            textBox1.Text = listView1.SelectedItems[0].SubItems[2].Text;
            numericUpDown1.Value = Convert.ToInt32(listView1.SelectedItems[0].SubItems[3].Text);
        }

        private void listView2_MouseClick(object sender, MouseEventArgs e)
        {
            id_goscia = listView2.SelectedItems[0].SubItems[0].Text;
            textBox5.Text = listView2.SelectedItems[0].SubItems[1].Text + " " + listView2.SelectedItems[0].SubItems[2].Text;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {            
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieRezerwacja = new MySqlCommand("update 18992403_hotel.tbRezerwacje set Id_rezerwacji = '" + _id_rezerwacji + "',Id_pokoju = '" + id_pokoju + "',Id_goscia = '" + id_goscia + "',Data_rozpoczecia = '" + dateTimePicker1.Value.ToShortDateString() + "',Data_zakonczenia = '" + dateTimePicker2.Value.ToShortDateString() + "',Liczba_osob = '" + numericUpDown1.Value + "' where Id_rezerwacji = '" + _id_rezerwacji + "'; ", Polaczenie);
                MySqlDataReader CzytanieRezerwacja;

                try
                {
                    Polaczenie.Open();
                    CzytanieRezerwacja = ZapytanieRezerwacja.ExecuteReader();
                    MessageBox.Show("Zaktualizowano");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();         
            
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
