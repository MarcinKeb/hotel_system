﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Hotel
{
    public partial class Form1 : Form
    {
        string Polaczenie_z_baza = "datasource=serwer1505592.home.pl;port=3306;username=18992403_hotel;password=H3Ha0sI9AUp5;charset=utf8";
       
        public Form1()
        {
            InitializeComponent();            
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text != "") && (maskedTextBox1.Text != ""))
            {
                try
                {
                    MySqlConnection TPracownicy = new MySqlConnection(Polaczenie_z_baza);
                    MySqlCommand ZapytaniePracownik = new MySqlCommand("select * from 18992403_hotel.tbUzytkownicy where login='" + this.textBox1.Text + "'and haslo='" + this.maskedTextBox1.Text + "'and aktywny_uzytkownik=1 ;", TPracownicy);
                    MySqlDataReader CzytanieLoginHaslo;
                    TPracownicy.Open();
                    CzytanieLoginHaslo = ZapytaniePracownik.ExecuteReader();

                    int Pracownik = 0;
                    while (CzytanieLoginHaslo.Read())
                    {
                        Pracownik += 1;
                    }
                    if (Pracownik == 1)
                    {
                        this.Hide();

                        string id = CzytanieLoginHaslo.GetString("id_uzytkownika");
                        Form2 FormaDruga = new Form2(id);
                        FormaDruga.ShowDialog();
                    }
                    else
                    {
                        label4.Text = "Nieprawidłowy login lub hasło";
                        textBox1.BackColor = Color.Salmon;
                        maskedTextBox1.BackColor = Color.Salmon;
                    }
                    TPracownicy.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                label4.Text = "Wprowadź login i hasło";
                textBox1.BackColor = Color.Salmon;
                maskedTextBox1.BackColor = Color.Salmon;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            label4.Text = "Wprowadź dane do logowania";
            textBox1.BackColor = Color.White;
        }
            
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {
            maskedTextBox1.BackColor = Color.White;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand ZapytanieUsunGoscie = new MySqlCommand("DELETE FROM 18992403_hotel.tbGoscie;", Polaczenie);
            MySqlDataReader CzytanieUsunGoscie;

            MySqlConnection Polaczenie2 = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand ZapytanieUsunPokoje = new MySqlCommand("DELETE FROM 18992403_hotel.tbPokoje;", Polaczenie2);
            MySqlDataReader CzytanieUsunPokoje;
            try
            {
                Polaczenie.Open();
                Polaczenie2.Open();
                CzytanieUsunGoscie = ZapytanieUsunGoscie.ExecuteReader();
                CzytanieUsunPokoje = ZapytanieUsunPokoje.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Polaczenie.Close();
            Polaczenie2.Close();

            Random losuj = new Random();

            int petla = losuj.Next(5, 20);
            for (int i = 0; i < petla; i++)
            {
                string imie;
            string[] imiona = { "Andrzej", "Anna", "Piotr", "Maria", "Krzysztof", "Katarzyna", "Małgorzata", "Jan", "Agnieszka", "Stanisław",
                                "Barbara", "Tomasz", "Paweł", "Łukasz", "Ewa", "Marcin", "Zofia", "Teresa", "Józef", "Monika"};
            imie = imiona[losuj.Next(0, 20)];

            string nazwisko;
            string[] nazwiska = { "Nowak", "Kowalski", "Wiśniewski", "Dąbrowski", "Lewandowski", "Wójcik", "Kamiński", "Kowalczyk", "Zieliński", "Szymański",
                                "Wojciechowski", "Kwiatkowski", "Woźniak", "Kozłowski", "Mazur", "Krawczyk", "Adamczyk", "Dudek", "Zając", "Król"};
            nazwisko = nazwiska[losuj.Next(0, 20)];

            string PESEL;
            PESEL = Convert.ToString(losuj.Next(50, 97) + "0" + losuj.Next(1, 10) + losuj.Next(10, 28) + losuj.Next(10000, 30000));

            string ulica;
            string[] ulice = { "Polna", "Leśna", "Słoneczna", "Krótka", "Długa", "Szkolna", "Ogrodowa", "Lipowa", "Brzozowa", "Łąkowa",
                                "Kwiatowa", "Kościelna", "Rynek", "Armi Krajowej", "3-go Maja", "Jana Pawła II", "Kościuszki", "Strzegomska", "Sejmowa", "Jaworzyńska"};
            ulica = ulice[losuj.Next(0, 20)];

            string nr_domu;
            string[] numery = { "A", "B", "C", "", "D", "E", "F", "", "G", "", "H" };
            nr_domu = Convert.ToString(losuj.Next(1, 150)) + numery[losuj.Next(0, 11)];

            string miasto;
            int indeks_miasta;
            string[] miasta = { "Strzegom", "Legnica", "Świdnica", "Wrocław", "Warszawa", "Kraków", "Łódź", "Poznań", "Gdańsk", "Szczecin",
                                "Bydgoszcz", "Lublin", "Katowice", "Gdyna", "Radom", "Zgorzelec", "Toruń", "Kielce"};
            indeks_miasta = losuj.Next(0, 18);
            miasto = miasta[indeks_miasta];

            string kod_poczt;
            string[] kody = { "58-120", "59-200", "58-150", "50-215", "00-001", "30-435", "93-137", "60-734", "80-333", "70-002",
                                "85-027", "20-001", "43-302", "81-004", "26-600", " 59-903", "87-118", "25-004"};
            kod_poczt = kody[indeks_miasta];

            string telefon;
            telefon = Convert.ToString(losuj.Next(600, 999) + "" + losuj.Next(100000, 999999));

            MySqlConnection Polaczenie3 = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand ZapytanieDodajGoscie = new MySqlCommand("insert into 18992403_hotel.tbGoscie (imie,nazwisko,PESEL,ulica,nr_domu,miasto,kod_pocztowy,telefon) values('" + imie + "','" + nazwisko + "','" + PESEL + "','" + ulica + "','" + nr_domu + "','" + miasto + "','" + kod_poczt + "','" + telefon + "') ;", Polaczenie3);
            MySqlDataReader CzytanieDodajGoscie;

            try
            {
                Polaczenie3.Open();
                CzytanieDodajGoscie = ZapytanieDodajGoscie.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Polaczenie3.Close();
        }

            int nr_pokoju = 0;
            int liczba_miejsc = 0;
            petla = losuj.Next(5, 20);

            for (int i = 0; i < petla; i++)
            {
                int nr_pietra = losuj.Next(0, 4);
                switch (nr_pietra) {
                    case 0: nr_pokoju = losuj.Next(1, 100); break;
                    case 1: nr_pokoju = losuj.Next(100, 200); break;
                    case 3: nr_pokoju = losuj.Next(200, 300); break;
                    case 4: nr_pokoju = losuj.Next(300, 400); break;
                }
                               
                liczba_miejsc = losuj.Next(1, 5);
                MySqlConnection Polaczenie4 = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieDodajPokoje = new MySqlCommand("insert into 18992403_hotel.tbPokoje (nr_pokoju,nr_pietra,liczba_miejsc) values('" + nr_pokoju + "','" + nr_pietra + "','" + liczba_miejsc + "') ;", Polaczenie4);
                MySqlDataReader CzytanieDodajPokoje;

                try
                {
                    Polaczenie4.Open();
                    CzytanieDodajPokoje = ZapytanieDodajPokoje.ExecuteReader();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie4.Close();
            }

            MessageBox.Show("Dodano przykładowe dane");
        }
    }
}
