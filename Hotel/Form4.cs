﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Hotel
{
    public partial class Form4 : Form
    {
        string Polaczenie_z_baza = "datasource=serwer1505592.home.pl;port=3306;username=18992403_hotel;password=H3Ha0sI9AUp5;charset=utf8";
        string id_pokoju;
        string id_goscia;
        public Form4(string _id_pokoju, string _numer_pokoju, string _ilosc_miejsc, string _data_rozpocz, string _data_zakon)
        {
            id_pokoju = _id_pokoju;
            InitializeComponent();
            textBox1.Text = _numer_pokoju;
            numericUpDown1.Value = Convert.ToInt32(_ilosc_miejsc);
            dateTimePicker1.Text = _data_rozpocz;
            dateTimePicker2.Text = _data_zakon;
            Pokaz_tabele_Pokoje();
            Pokaz_tabele_Goscie();
        }

        void Pokaz_tabele_Pokoje()
        {
            listView1.Items.Clear();
            listView1.Columns[0].Width = 30;
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytaniePokoje = new MySqlCommand("select * from 18992403_hotel.tbPokoje where istniejacy_pokoj=1 ORDER BY nr_pietra;", Polaczenie);
                MySqlDataReader CzytaniePokoje;
                Polaczenie.Open();
                CzytaniePokoje = ZapytaniePokoje.ExecuteReader();

                while (CzytaniePokoje.Read())
                {
                    ListViewItem item = new ListViewItem(CzytaniePokoje["id_pokoju"].ToString());
                    item.SubItems.Add(CzytaniePokoje["nr_pietra"].ToString());
                    item.SubItems.Add(CzytaniePokoje["nr_pokoju"].ToString());
                    item.SubItems.Add(CzytaniePokoje["liczba_miejsc"].ToString());

                    listView1.Items.Add(item);
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Pokaz_tabele_Goscie()
        {
            listView2.Items.Clear();
            listView2.Columns[0].Width = 30;
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieGoscie = new MySqlCommand("select * from 18992403_hotel.tbGoscie where aktywny_gosc=1;", Polaczenie);
                MySqlDataReader CzytanieGoscie;
                Polaczenie.Open();
                CzytanieGoscie = ZapytanieGoscie.ExecuteReader();

                while (CzytanieGoscie.Read())
                {
                    ListViewItem item = new ListViewItem(CzytanieGoscie["id_goscia"].ToString());
                    item.SubItems.Add(CzytanieGoscie["imie"].ToString());
                    item.SubItems.Add(CzytanieGoscie["nazwisko"].ToString());
                    item.SubItems.Add(CzytanieGoscie["PESEL"].ToString());
                    item.SubItems.Add(CzytanieGoscie["ulica"].ToString());
                    item.SubItems.Add(CzytanieGoscie["nr_domu"].ToString());
                    item.SubItems.Add(CzytanieGoscie["miasto"].ToString());
                    item.SubItems.Add(CzytanieGoscie["kod_pocztowy"].ToString());
                    item.SubItems.Add(CzytanieGoscie["telefon"].ToString());

                    listView2.Items.Add(item);
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);            
            MySqlCommand ZapytanieRezerwacja = new MySqlCommand("insert into 18992403_hotel.tbRezerwacje (Id_pokoju,Id_goscia,Data_rozpoczecia,Data_zakonczenia,Liczba_osob) values('" + id_pokoju + "','" + id_goscia + "','" + dateTimePicker1.Value.ToShortDateString() + "','" + dateTimePicker2.Value.ToShortDateString() + "','" + numericUpDown1.Value + "') ;", Polaczenie);

            MySqlDataReader CzytanieRezerwacja;
            try
            {
                Polaczenie.Open();
                CzytanieRezerwacja = ZapytanieRezerwacja.ExecuteReader();
                MessageBox.Show("Dodano rezerwację");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Polaczenie.Close();

            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            id_pokoju = listView1.SelectedItems[0].SubItems[0].Text;
            textBox1.Text = listView1.SelectedItems[0].SubItems[2].Text;
            numericUpDown1.Value = Convert.ToInt32(listView1.SelectedItems[0].SubItems[3].Text);
        }

        private void listView2_MouseClick(object sender, MouseEventArgs e)
        {
            id_goscia = listView2.SelectedItems[0].SubItems[0].Text;
            textBox5.Text = listView2.SelectedItems[0].SubItems[1].Text + " " + listView2.SelectedItems[0].SubItems[2].Text;
            pictureBox1.Enabled = true;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {            
            listView2.Items.Clear();
            listView2.Columns[0].Width = 30;
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieGoscie = new MySqlCommand("select id_goscia,imie,nazwisko,PESEL,ulica,nr_domu,miasto,kod_pocztowy,telefon from 18992403_hotel.tbGoscie where aktywny_gosc=1 and nazwisko like '" + textBox2.Text + "%" + "' ;", Polaczenie);
                MySqlDataReader CzytanieGoscie;
                Polaczenie.Open();
                CzytanieGoscie = ZapytanieGoscie.ExecuteReader();

                while (CzytanieGoscie.Read())
                {
                    ListViewItem item = new ListViewItem(CzytanieGoscie["id_goscia"].ToString());
                    item.SubItems.Add(CzytanieGoscie["imie"].ToString());
                    item.SubItems.Add(CzytanieGoscie["nazwisko"].ToString());
                    item.SubItems.Add(CzytanieGoscie["PESEL"].ToString());
                    item.SubItems.Add(CzytanieGoscie["ulica"].ToString());
                    item.SubItems.Add(CzytanieGoscie["nr_domu"].ToString());
                    item.SubItems.Add(CzytanieGoscie["miasto"].ToString());
                    item.SubItems.Add(CzytanieGoscie["kod_pocztowy"].ToString());
                    item.SubItems.Add(CzytanieGoscie["telefon"].ToString());

                    listView2.Items.Add(item);
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
