﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Hotel
{
    public partial class Form5 : Form
    {
        string id_uzytkownika;
        string Polaczenie_z_baza = "datasource=serwer1505592.home.pl;port=3306;username=18992403_hotel;password=H3Ha0sI9AUp5;charset=utf8";

        public Form5()
        {
            InitializeComponent();
            Pokaz_tabele_Uzytkownicy();
        }

        void Pokaz_tabele_Uzytkownicy()
        {
            listView2.Items.Clear();
            listView2.Columns[0].Width = 30;
            pictureBox1.Enabled = false;
            pictureBox2.Enabled = false;
            try
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieUzytkownicy = new MySqlCommand("select * from 18992403_hotel.tbUzytkownicy where aktywny_uzytkownik=1;", Polaczenie);
                MySqlDataReader CzytanieUzytkownicy;
                Polaczenie.Open();
                CzytanieUzytkownicy = ZapytanieUzytkownicy.ExecuteReader();

                while (CzytanieUzytkownicy.Read())
                {
                    ListViewItem item = new ListViewItem(CzytanieUzytkownicy["id_uzytkownika"].ToString());
                    item.SubItems.Add(CzytanieUzytkownicy["imie"].ToString());
                    item.SubItems.Add(CzytanieUzytkownicy["nazwisko"].ToString());
                    item.SubItems.Add(CzytanieUzytkownicy["stanowisko"].ToString());
                    item.SubItems.Add(CzytanieUzytkownicy["login"].ToString());
                    item.SubItems.Add(CzytanieUzytkownicy["haslo"].ToString());

                    listView2.Items.Add(item);
                }
                Polaczenie.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void listView2_MouseClick(object sender, MouseEventArgs e)
        {
            id_uzytkownika = listView2.SelectedItems[0].SubItems[0].Text;
            textBox4.Text = listView2.SelectedItems[0].SubItems[1].Text;
            textBox6.Text = listView2.SelectedItems[0].SubItems[2].Text;
            textBox8.Text = listView2.SelectedItems[0].SubItems[3].Text;
            textBox10.Text = listView2.SelectedItems[0].SubItems[4].Text;
            textBox1.Text = listView2.SelectedItems[0].SubItems[5].Text;
            pictureBox1.Enabled = true;
            pictureBox2.Enabled = true;
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            if ((textBox4.Text != "") && (textBox6.Text != "") && (textBox8.Text != "") && (textBox10.Text != "") && (textBox1.Text != ""))
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieUzytkownicy = new MySqlCommand("insert into 18992403_hotel.tbUzytkownicy (imie,nazwisko,stanowisko,login,haslo) values('" + this.textBox4.Text + "','" + this.textBox6.Text + "','" + this.textBox8.Text + "','" + this.textBox10.Text + "','" + this.textBox1.Text + "') ;", Polaczenie);
                MySqlDataReader CzytanieUzytkownicy;

                try
                {
                    Polaczenie.Open();
                    CzytanieUzytkownicy = ZapytanieUzytkownicy.ExecuteReader();
                    MessageBox.Show("Zapisano");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Uzytkownicy();
            }
            else
            {
                if (textBox4.Text == "") textBox4.BackColor = Color.Salmon;
                if (textBox6.Text == "") textBox6.BackColor = Color.Salmon;
                if (textBox8.Text == "") textBox8.BackColor = Color.Salmon;
                if (textBox10.Text == "") textBox10.BackColor = Color.Salmon;
                if (textBox1.Text == "") textBox1.BackColor = Color.Salmon;
                MessageBox.Show("Wprowadź dane");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if ((textBox4.Text != "") && (textBox6.Text != "") && (textBox8.Text != "") && (textBox10.Text != "") && (textBox1.Text != ""))
            {
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieUzytkownicy = new MySqlCommand("update 18992403_hotel.tbUzytkownicy set id_uzytkownika= '" + id_uzytkownika + "',imie = '" + textBox4.Text + "',nazwisko = '" + textBox6.Text + "',stanowisko = '" + textBox8.Text + "',login = '" + textBox10.Text + "',haslo = '" + textBox1.Text + "' where id_uzytkownika= '" + id_uzytkownika + "'; ", Polaczenie);
                MySqlDataReader CzytanieUzytkownicy;

                try
                {
                    Polaczenie.Open();
                    CzytanieUzytkownicy = ZapytanieUzytkownicy.ExecuteReader();
                    MessageBox.Show("Zaktualizowano");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Uzytkownicy();
            }
            else
            {
                if (textBox4.Text == "") textBox4.BackColor = Color.Salmon;
                if (textBox6.Text == "") textBox6.BackColor = Color.Salmon;
                if (textBox8.Text == "") textBox8.BackColor = Color.Salmon;
                if (textBox10.Text == "") textBox10.BackColor = Color.Salmon;
                if (textBox1.Text == "") textBox1.BackColor = Color.Salmon;
                MessageBox.Show("Wprowadź dane");
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {            
                MySqlConnection Polaczenie = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieUzytkownicy = new MySqlCommand("update 18992403_hotel.tbUzytkownicy set aktywny_uzytkownik=0 where id_uzytkownika= '" + id_uzytkownika + "'; ", Polaczenie);
                MySqlDataReader CzytanieUzytkownicy;

                try
                {
                    Polaczenie.Open();
                    CzytanieUzytkownicy = ZapytanieUzytkownicy.ExecuteReader();
                    MessageBox.Show("Usunięto");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Polaczenie.Close();
                Pokaz_tabele_Uzytkownicy();
            }
            
        }
    }

